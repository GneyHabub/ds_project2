import fs from 'fs';
import {yellow} from 'kleur';
import {IncomingMessage, ServerResponse} from 'http';

const readFile = (request: IncomingMessage, response: ServerResponse, whereToPut: string) => {
  try {
    if (request.method === 'POST') {
      const filename = request.headers['filename'] as string || '';
      if (filename) {
        console.log('writing the file', filename);
        const stream = fs.createWriteStream(whereToPut + '/' + filename);
        request.pipe(stream);

        // After all the data is saved, respond with a simple html form so they can post more data
        request.on('end', function () {
          console.log(`File is written into ${yellow(whereToPut + '/' + filename)}`);
          response.statusCode = 204;
          response.end();
        });

        // This is here in case any errors occur
        stream.on('error', function (err) {
          response.statusCode = 500;
          response.end(JSON.stringify({error: 'An error occurred during file upload:' + err}));
        });
      } else {
        response.statusCode = 400;
        response.end(JSON.stringify({error: 'Proper request headers are not provided'}));
      }
    } else {
      response.statusCode = 404;
      response.end(JSON.stringify({error: 'Wrong url or method of the request'}));
    }
  } catch (e) {
    response.statusCode = 500;
    response.end(JSON.stringify({error: 'An error occurred during file upload:' + e}));
  }
}

export default readFile;
