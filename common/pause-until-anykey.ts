import { red } from 'kleur';

const pauseUntilAnykey = (someTextOrCallback: string | (() => void), cb: () => void = () => {}) => {
  const text = someTextOrCallback && typeof someTextOrCallback === 'string' ? someTextOrCallback : null;
  const callback = typeof someTextOrCallback === 'function' ? someTextOrCallback : cb;

  if (text) console.log(red(text));
  console.log('To continue, press any key.');
  process.stdin.setRawMode(true);
  process.stdin.resume();
  process.stdin.once('data', () => callback());
};
export default pauseUntilAnykey;
