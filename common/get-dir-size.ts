import fs from 'fs';
import path from 'path';

/**
 * Get the size (in bytes) of all files in the directory. It goes through it recursively
 * @param directoryPath
 */
const getDirSize = (directoryPath: string) => {
  const arrayOfFiles = getAllFiles(directoryPath);

  let totalSize = 0;

  arrayOfFiles.forEach((filePath) => {
    totalSize += fs.statSync(filePath).size;
  });

  return totalSize;
};

const getAllFiles = function(dirPath: string, arrayOfFiles: string[] = []): string[] {
  const files = fs.readdirSync(dirPath);

  arrayOfFiles = arrayOfFiles || [];

  files.forEach((file) => {
    if (fs.statSync(dirPath + '/' + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
    } else {
      arrayOfFiles.push(path.join(dirPath, file));
    }
  })

  return arrayOfFiles;
};

export default getDirSize;
