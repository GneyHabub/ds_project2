import net from 'net';

let PORT = 5000;
const getFreePort = (cb: (port: number) => void) => {
  let port = PORT;
  PORT += 1;

  let server = net.createServer();
  server.listen(port, () => {
    server.once('close',  () => {
      cb(port);
    });
    server.close();
  });
  server.on('error', () => {
    getFreePort(cb);
  })
}

export default getFreePort;
