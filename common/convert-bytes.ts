/**
 * An util that transforms the amount of bytes to something like 2.4MB or 8.2GB
 * @param bytes {number}
 */
const convertBytes = (bytes: number) => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']

  if (bytes == 0) return 'n/a';

  const i = parseInt((Math.floor(Math.log(bytes) / Math.log(1024)).toString()));

  if (i == 0) return bytes + ' ' + sizes[i];
  return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i]
}

export default convertBytes;
