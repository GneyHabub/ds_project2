import {IncomingMessage, ServerResponse} from 'http';
import parse from './request-parse';

type RequestHandleSettings = {
  method: 'GET'|'POST'|'PUT'|'DELETE',
  condition: (body?: any) => boolean|null,
  400?: string,
  404?: string,
  500?: string,
};

const DEFAULT_SETTINGS: RequestHandleSettings = {
  method: 'POST',
  condition: () => null,
  400: 'Wrong data passed to request',
  404: 'Wrong url or method of the request',
  500: 'Some error happened on the server',
}

/**
 * Use as a wrapper of HTTP request with any method
 * @param settings
 * @param request
 * @param response
 * @param cb – callback to use if no error is found
 */
const requestHandle = (
  settings: RequestHandleSettings = DEFAULT_SETTINGS,
  request: IncomingMessage,
  response: ServerResponse,
  cb: (body: any) => void
) => {
  try {
    if (request.method === settings.method) {
      parse(request, response, (body) => {
        if (settings.condition(body)) {
          cb(body);
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: settings['400']}));
        }
      });
    } else {
      response.statusCode = 404;
      response.end(JSON.stringify({error: settings['404']}));
    }
  } catch (e) {
    response.statusCode = 500;
    response.end(JSON.stringify({error: settings['500']}));
  }
};
export default requestHandle;
