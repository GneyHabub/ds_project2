import { type } from "os";

export type Question = {
  type: 'input'|'number'|'confirm'|'list'|'rawlist'|'expand'|'checkbox'|'password'|'editor',
  name: string,
  message: string | ((answer: Answer) => string),
  default?: string|number|boolean,
  choices?: string[],
  validate?: (input: string, answer: Answer) => boolean,
  prefix?: string,
  suffix?: string,
};

export type Answer = {
  [key: string]: string,
}

export type FileServerRecord = {
  ip: string,
  token: string,
  capacity: number,
  free: number,
}

export type ClientRecord = {
  ip: string,
  username: string,
  position: string, // Position in the DFS. Stored like '/root/user/smth'
}

export type NameServerRecord = {
  ip: string,
  token: string,
}

export type Map = {
  rootDir: Directory,
}

export type Directory = {
  name: string;
  contents: (Directory | File)[];
};

export function isDir(test: any): test is Directory {
  return test.contents !== undefined;
}

export type File = {
  name: string;
  size: number;
  servers: string[];
}

export function isFile(test: any): test is File {
  return test.servers !== undefined && test.size !== undefined;
}

export type clientGlobalObject = {
  nameServer: NameServerRecord,
  rootDir: string,
  username: string,
}

export type StorageGlobalObject = {
  storage_path: string|null,
  ip: string|null,
  port: number|null,
  token: string|null,

  ns_ip: string|null,
  capacity: number|null,
  map: Map|null,
  servers: FileServerRecord[],
}

export type ServerGlobalObject = {
  map: Map,
  PORT: number | null,
  ip: string | null,
  TOKEN: string | null,
  fileServers: FileServerRecord[],
  trustedHosts: ClientRecord[],
}

export type ActiveProcess = {
  id: string,
  client: ClientRecord,
  servers: FileServerRecord[],
  file: string,
  size: number,
  started: number, // timestamp Date.now()
}
