import os from 'os';

const getIp = (): string|null => {
  const interfaces = os.networkInterfaces();
  const values = Object.values(interfaces) || [];
  const ips = values
    .flat()
    .filter((item) => item && !item.internal && item.family === 'IPv4')
    .find(Boolean)

  return ips ? ips.address : null;
};
export default getIp;
