import {Response} from 'node-fetch';
import {red} from 'kleur';

const requestError = async (response: Response, cb: (body?: any) => void) => {
  if (response.ok) {
    if (response.status === 200) {
      const body = await response.json();
      cb(body);
    } else {
      cb();
    }
  } else if (response.status === 500) {
      console.error(red('An error occurred on the server.'));
  } else if (response.status === 400) {
    try {
      const body = await response.json();
      console.log(red(body.error));
    } catch (_) {
      console.log('Something wrong happened.');
    }
  }
};

export default requestError;
