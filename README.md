# PUPA & LUPA DFS
See [spec.md](./spec.md) for information about the internal architecture of the system

Distributed file system project of the team Pupa & Lupa.

## Initialization

Assuming you have Node.js installed, run in your directory:

```bash
npm ci
```

And then, depending on the type of what you need to install (client|server|storage), do like:

```bash
npm run client
```

## Using docker image

Depending on the type of the DFS instance you need, do:

```bash
# client|server|storage specified in the end
docker pull vanishmax/ds-proj-client:1
docker run vanishmax/ds-proj-client:1
```

Additional information: it is better to run images in the sequence:
storage -> server -> client. It would connect them in proper order. 

## Team contribution

The work in team Pupa and Lupa was splitted equally between Alexandr Krivonosov and Maxim Korsunov. We were assigning each others issues in repository, discussing every project's detail and writing a lot of code.

Commit statistics by contributor can be seen [here](https://gitlab.com/GneyHabub/ds_project2/-/graphs/master)
