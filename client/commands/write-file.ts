import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';
import {red, green} from 'kleur';
import {prompt} from 'enquirer';
import requestError from '../../common/request-error';
import {Answer, FileServerRecord} from '../../common/types';
import {global} from '../global';
import ora = require('ora');
import pauseUntilAnykey from '../../common/pause-until-anykey';
import {performOperations} from '../operations';

type WriteFileResponseBody = {
  process: string,
  success: boolean,
  servers?: FileServerRecord[],
  name?: string,
  path: string,
};

const writeFile = async () => {
  const {where}: Answer = await prompt({
    type: 'input',
    name: 'where',
    message: 'Provide the path to the file to put into active directory:',
    validate: (input) => !!input,
  });

  fs.lstat(where, async (err, stat) => {
    if (err) console.log(red(`An error occurred: ${err}.\nPlease try again.`));

    const requestBody = {
      token: global.nameServer.token,
      username: global.username,
      name: path.basename(where),
      size: stat.size,
    };

    const res = await fetch(`http://${global.nameServer.ip}/write-file`, {
      method: 'POST',
      body: JSON.stringify(requestBody),
    });

    await requestError(res, async (body: WriteFileResponseBody) => {
      if (!body.success) {
        await applyActions(requestBody, where, stat.size);
      } else {
        await sendFile(body, where, stat.size);
      }
    });
  });
};

const applyActions = async (body: {token: string, name: string, size: number}, filePath: string, size: number) => {
  const {select}: Answer = await prompt({
    type: 'select',
    name: 'select',
    message: 'File with such name exists. What to do with it?',
    choices: ['override', 'copy', 'stop'],
  });

  if (select === 'stop') return;

  const res = await fetch(`http://${global.nameServer.ip}/write-file`, {
    method: 'POST',
    body: JSON.stringify({...body,username: global.username, action: select}),
  });

  await requestError(res, async (body: WriteFileResponseBody) => {
    await sendFile(body, filePath, size);
  });
};

const sendFile = async (body: WriteFileResponseBody, filePath: string, size: number) => {
  if (body.servers) {
    const stream = fs.createReadStream(filePath);

    const spinner = ora();
    spinner.text = 'Uploading the file: ' + body.name;
    spinner.start();

    try {
      const res = await Promise.all(body.servers.map(async (server) => {
        const response = await fetch(`http://${server.ip}/write-file`, {
          method: 'POST',
          headers: {
            filesize: size.toString(),
            filename: body.name || path.basename(filePath),
            clientpath: body.path,
            servertoken: server.token,
          },
          body: stream,
        });
        return {success: response.ok, ip: server.ip};
      }));

      spinner.stop();
      await sendAck({servers: res, process: body.process});
    } catch (e) {
      spinner.stop();
      console.log(red('An error occurred while uploading your file: ' + e));
      await sendAck({
        servers: body.servers.map((server) => ({ip: server.ip, success: false})),
        process: body.process,
      });
    }
  }
};

const sendAck = async ({servers, process}: {
  servers: {success: boolean, ip: string}[],
  process: string,
}) => {
  const response = await fetch(`http://${global.nameServer.ip}/write-file-ack`, {
    method: 'POST',
    body: JSON.stringify({servers, process, token: global.nameServer.token, username: global.username}),
  });

  await requestError(response,  () => {
    console.log(green('The file is successfully uploaded!'));
    pauseUntilAnykey(performOperations);
  });
};

export default writeFile;
