import { bold, red, green, yellow } from 'kleur';
import { createServer, IncomingMessage, ServerResponse } from 'http';
import {prompt} from 'enquirer';
import {Answer, NameServerRecord, File, Directory, FileServerRecord, ServerGlobalObject} from '../common/types';
import fetch from 'node-fetch';
import {global, downloadConf} from './global';
import pauseUntilAnykey from '../common/pause-until-anykey';
import path from 'path';
import fs from 'fs';
import requestError from '../common/request-error';
import writeFile from './commands/write-file';
import readFile from '../common/read-file';

export const cache = {
  cachePath: path.join(__dirname, 'cache.txt'),
  // Get the global object from the cache.txt file and apply it to the same object
  get (exists: (contents: ServerGlobalObject) => void, notExists: () => void) {
    if (fs.existsSync(this.cachePath)) {
      const contents: ServerGlobalObject = JSON.parse(fs.readFileSync(this.cachePath, 'utf8'));
      Object.keys(global).forEach((key) => {
        // @ts-ignore
        if (contents[key]) global[key] = contents[key];
      });
      exists(contents);
    } else {
      notExists();
    }
  },
  // Set active global object to the cache.txt file
  set () {
    fs.writeFileSync(this.cachePath, JSON.stringify(global));
  },
};

export async function performOperations () {
  const response: { command: keyof typeof operations } = await prompt({
    type: 'autocomplete',
    name: 'command',
    message: 'Command:',
    initial: 0,
    choices: Object.keys(operations),
  });

  process.on('uncaughtException', function (err) {
    pauseUntilAnykey('An error happened: ' + err, performOperations);
  });
  process.on('unhandledRejection', (err) => {
    pauseUntilAnykey('An error happened: ' + err, performOperations);
  });

  await operations[response.command]();
  if (response.command !== 'write_file') {
    pauseUntilAnykey(performOperations);
  }
}

export const initialize = async () => {
  cache.get(async () => {
    try {
      const res = await fetch(`http://${global.nameServer.ip}/client-re-auth`, {method: 'POST'});
      if (res.ok && res.status === 200) {
        const data: {ip: string, token: string, firstUser: boolean, rootDir: string} = await res.json();

        global.nameServer.ip = data.ip;
        global.nameServer.token = data.token;

        // If client is the first, set root directory
        if (data.firstUser) {
          await setRootDir();
        } else {
          global.rootDir = data.rootDir;
          cache.set();
          console.log(green('Successfully connected to the NameServer!'));
          await help();
        }
      } else {
        await register();
      }
    } catch (_) {
      // TODO: posssibly, NameServer is dead here!
      await register();
    }
  }, async () => {
    await register();
  });
};

export async function register () {
  const {name}: Answer = await prompt({
    type: 'input',
    name: 'name',
    message: 'What is your name?',
    validate: (input) => !!input,
  });
  console.log(green(`Hi, ${name}`));
  global.username = name;

  const {ip}: Answer = await prompt({
    type: 'input',
    name: 'ip',
    message: 'Type the ip address and the port of the NameServer:',
    validate: (input) => !!input,
  });
  const {token}: Answer = await prompt({
    type: 'input',
    name: 'token',
    message: 'Type the token provided by the NameServer:',
    validate: (input) => !!input,
  });

  const response = await fetch(`http://${ip}/client-auth`, {
      method: 'POST',
      body: JSON.stringify({token, username: global.username}),
  });

  await requestError(response, async (body: {firstUser?: boolean, error?: string, rootDir: string}) => {
    global.nameServer = {
      ip,
      token,
    };

    if (body.firstUser) {
      // Create root dir
      await setRootDir();
    } else {
      global.rootDir = body.rootDir;
      console.log(green('Successfully connected to the NameServer!'));
      cache.set();

      await help();
    }
  });
}

const setRootDir = async () => {
  const res3: Answer = await prompt({
    type: 'input',
    name: 'rootDirName',
    message: 'Type the name of the root directory of the DFS:',
    validate: (input) => !!input,
  });
  const rootCreateResp = await fetch(`http://${global.nameServer.ip}/create-root-dir`, {
    method: 'POST',
    body: JSON.stringify({token: global.nameServer.token, rootDirName: res3.rootDirName, username: global.username}),
  });
  if (rootCreateResp.status === 204) {
    global.rootDir = res3.rootDirName;
    console.log(`Root directory ${yellow(global.rootDir)} is created!`);
    cache.set();

    await help();
  } else if (rootCreateResp.status === 500) {
    console.log('An error occurred on the NameServer.');
  } else if (rootCreateResp.status === 400) {
    try {
      const body = await rootCreateResp.json();
      console.log(body.error);
    } catch (_) {
      console.log('Something wrong happened.');
    }
  }
  const server = createServer((request: IncomingMessage, response: ServerResponse) => {
    switch (request.url) {
      case '/get-read-file': {
        // The function is common because it is also used in the file server
        readFile(request, response, downloadConf.path);
        break;
      }
    }
  });
  server.listen(downloadConf.PORT);
}

export async function help () {
  const response: { confirm: boolean } = await prompt({
    type: 'confirm',
    name: 'confirm',
    message: 'Do you need help?',
  });

  if (response.confirm) {
    console.log(`
${bold('create_file')}: Create a new empty file
${bold('read_file')}: Read any file from DFS
${bold('write_file')}: Put any file to DFS
${bold('delete_file')}: Delete any file from DFS
${bold('info_file')}: Information about the file
${bold('copy_file')}: Create a copy of file
${bold('move_file')}: Move a file to the specified path
${bold('open_dir')}: Change directory
${bold('list_dir')}: Return list of files, which are stored in the directory
${bold('make_dir')}: Create a new directory
${bold('delete_dir')}: Delete directory
`);
    pauseUntilAnykey(performOperations);
  } else {
    await performOperations();
  }
}

const operations = {
  create_file: async () => {
    const response: Answer = await prompt({
      type: 'input',
      name: 'fileName',
      message: 'Provide the name of the file to be created:',
      validate: (input) => !!input,
    });
    const dirCreateResp = await fetch(`http://${global.nameServer.ip}/create-file`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, fileName: response.fileName, username: global.username}),
    });
    if (dirCreateResp.status === 204) {
      console.log(`File ${yellow(response.fileName)} is created!`);
    } else if (dirCreateResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (dirCreateResp.status === 400) {
      try {
        const body = await dirCreateResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  read_file: async () => {
    const fileName: Answer = await prompt({
      type: 'input',
      name: 'name',
      message: 'Provide the name of the file to be downloaded:',
      validate: (input) => !!input,
    });
    const path: Answer = await prompt({
      type: 'input',
      name: 'path',
      message: 'Provide the path to a local directory, where the file is to be stored:',
      validate: (input) => !!input,
    });

    downloadConf.path = path.path;

    const response = await fetch(`http://${global.nameServer.ip}/read-file`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, username: global.username, fileName: fileName.name, port: downloadConf.PORT}),
    });

    if (response.status === 204) {
      console.log(green(`Successfully downloaded ${yellow(fileName.name)}`));
    } else if (response.status === 500) {
      console.log(red('An error occurred at the NameServer.'));
    } else if (response.status === 400) {
      try {
        const body = await response.json();
        console.log(body.error);
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    } else {
      console.log("something went wrong!")
    }
  },

  write_file: writeFile,

  delete_file: async () => {
    const fileName: Answer = await prompt({
      type: 'input',
      name: 'name',
      message: 'Provide the name of the file to be deleted:',
      validate: (input) => !!input,
    });
    const deleteFileResp = await fetch(`http://${global.nameServer.ip}/delete-file`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, fileName: fileName.name, username: global.username}),
    });
    if (deleteFileResp.status === 204) {
      console.log(green(`Successfully deleted ${yellow(fileName.name)}`));
    } else if (deleteFileResp.status === 500) {
      console.log(red('An error occurred at the NameServer.'));
    } else if (deleteFileResp.status === 400) {
      try {
        const body = await deleteFileResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  info_file: async () => {
    const response: Answer = await prompt({
      type: 'input',
      name: 'fileName',
      message: 'Provide the name of the file:',
      validate: (input) => !!input,
    });
    const fileInfoResp = await fetch(`http://${global.nameServer.ip}/file-info`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, fileName: response.fileName, username: global.username}),
    });
    if (fileInfoResp.status === 200) {
      const body = await fileInfoResp.json();
      console.log(`Name: ${green(body.name)}`);
      console.log(`Size: ${green(body.size)}`);
    } else if (fileInfoResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (fileInfoResp.status === 400) {
      try {
        const body = await fileInfoResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  copy_file: async () => {
    const fileName: Answer = await prompt({
      type: 'input',
      name: 'name',
      message: 'Provide the name of the file to be copied:',
      validate: (input) => !!input,
    });
    const newPath: Answer = await prompt({
      type: 'input',
      name: 'path',
      message: `Provide the global path to the directory (starting with /${global.rootDir}), leaving empty if you want to save copy to the same directory:`,
    });
    const moveFileResp = await fetch(`http://${global.nameServer.ip}/move-file`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, fileName: fileName.name, targetDir: newPath.path, task: 'copy', username: global.username}),
    });
    if (moveFileResp.status === 204) {
      console.log(green(`Successfully copied ${yellow(fileName.name)} ${newPath ? 'into' + yellow(newPath.path) : ''}`));
    } else if (moveFileResp.status === 500) {
      console.log(red('An error occurred at the NameServer.'));
    } else if (moveFileResp.status === 400) {
      try {
        const body = await moveFileResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  move_file: async () => {
    const fileName: Answer = await prompt({
      type: 'input',
      name: 'name',
      message: 'Provide the name of the file to be moved:',
      validate: (input) => !!input,
    });
    const newPath: Answer = await prompt({
      type: 'input',
      name: 'path',
      message: `Provide the global path to the directory (starting with /${global.rootDir}):`,
      validate: (input) => !!input,
    });
    const moveFileResp = await fetch(`http://${global.nameServer.ip}/move-file`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, fileName: fileName.name, targetDir: newPath.path, task: 'move', username: global.username}),
    });
    if (moveFileResp.status === 204) {
      console.log(green(`Successfully moved ${yellow(fileName.name)} into ${yellow(newPath.path)}`));
    } else if (moveFileResp.status === 500) {
      console.log(red('An error occurred at the NameServer.'));
    } else if (moveFileResp.status === 400) {
      try {
        const body = await moveFileResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  open_dir: async () => {
    const response: Answer = await prompt({
      type: 'input',
      name: 'dirName',
      message: 'Provide the name of the directory:',
      validate: (input) => !!input,
    });
    const dirOpenResp = await fetch(`http://${global.nameServer.ip}/open-dir`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, dirName: response.dirName, username: global.username}),
    });
    if (dirOpenResp.status === 204) {
      console.log(green('Success!'));
    } else if (dirOpenResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (dirOpenResp.status === 400) {
      try {
        const body = await dirOpenResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  list_dir: async () => {
    const dirListResp = await fetch(`http://${global.nameServer.ip}/list-dir`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, username: global.username}),
    });
    if (dirListResp.status === 200) {
      const body = await dirListResp.json();
      body.contents.forEach((el: any) => {
        if (el.isDir) {
          console.log(green(el.name));
        } else {
          console.log(el.name);
        }
      })
    } else if (dirListResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (dirListResp.status === 400) {
      try {
        const body = await dirListResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  make_dir: async () => {
    const response: Answer = await prompt({
      type: 'input',
      name: 'dirName',
      message: 'Provide the name of the directory to be created:',
      validate: (input) => !!input,
    });
    const dirCreateResp = await fetch(`http://${global.nameServer.ip}/create-dir`, {
      method: 'POST',
      body: JSON.stringify({token: global.nameServer.token, dirName: response.dirName, username: global.username}),
    });
    if (dirCreateResp.status === 204) {
      console.log(`Directory ${yellow(response.dirName)} is created!`);
    } else if (dirCreateResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (dirCreateResp.status === 400) {
      try {
        const body = await dirCreateResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },

  delete_dir: async () => {
    const response: Answer = await prompt({
      type: 'input',
      name: 'dirName',
      message: 'Provide the name of the directory to be deleted:',
      validate: (input) => !!input,
    });
    const dirCreateResp = await fetch(`http://${global.nameServer.ip}/delete-dir`, {
      method: 'POST',
      body: JSON.stringify({
        token: global.nameServer.token,
        dirName: response.dirName,
        username: global.username,
      }),
    });
    if (dirCreateResp.status === 204) {
      console.log(`Directory ${yellow(response.dirName)} was ${red('deleted')}!`);
    } else if (dirCreateResp.status === 403) {
      const {confirm}: Answer = await prompt({
        type: 'confirm',
        name: 'confirm',
        message: 'Directory has content inside. Are you sure you want to delete it?',
      });
      if (confirm) {
        const dirCreateResp2 = await fetch(`http://${global.nameServer.ip}/delete-dir`, {
          method: 'POST',
          body: JSON.stringify({
            token: global.nameServer.token,
            dirName: response.dirName,
            username: global.username,
            confirm: true,
          }),
        });
        if (dirCreateResp2.status === 204) {
          console.log(`Directory ${yellow(response.dirName)} was ${red('deleted')}!`);
        } else {
          console.log(red('Something wrong happened.'));
        }
      }
    } else if (dirCreateResp.status === 500) {
      console.log(red('An error occurred on the NameServer.'));
    } else if (dirCreateResp.status === 400) {
      try {
        const body = await dirCreateResp.json();
        console.log(red(body.error));
      } catch (_) {
        console.log(red('Something wrong happened.'));
      }
    }
  },
}
