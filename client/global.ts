import { clientGlobalObject } from '../common/types';

export const global: clientGlobalObject = {
    nameServer: {
        ip: '',
        token: '',
    },
    rootDir: '',
    username: '',
};

export let downloadConf = {
    path: __dirname + '/files',
    PORT: 4001,
}
