import { bold } from 'kleur';
import {initialize} from './operations';

process.setMaxListeners(1000);

console.log(bold().yellow().italic(`
  Hello!
  To use the Pupa & Lupa DFS, we need to authorize you.
`));

(async function () {
  await initialize();
})();
