# Pupa and Lupa FS spec

## Init process:
1. Create NameServer. After that, it should ask user to add at least two FileServers.
2. Create at least 2 FileServers, generate unique tokens and pass them to the NameServer along with IPs of the FileServers. It will send messages to them, acknowledging about it's existance.
3. Lounch a Client. Make NameServer generate an auth token, and use it to connect Client to the NameServer.
4. NameServer adds Client to the list of trusred hosts, as well as Fileservers to the list of FileSerevers.
5. Create a root directory of the DFS via Client's UI.
6. NameServer initializes a data structure (Map) that maps the virtual file system presented to the user to the real files on the FileServers.
7. FileServers physically create the root directory on their hosts (all the directories will always be present on all of the FileServers).
8. If you have lounche NameServer and FileServers on the same machines before, then there's no need to pass tokens here and there - caching will take care of reconnecting all the components.

## Description of the actions

![](./static/diagram.png)

### Create/Write a new file
1. 
    * Run `create_file` command on the Client machine to create a new file in the current directory.
    * Or run `write_file` command on the Client machine to upload a new file to the current directory of the DFS.
2. Client sends a message to the NameServer with the request for allocation. It passes the name of the file along with the size of the file (zero for newly created files). NameServer checks for name collisions, picks the FileServers with the most disk capacity left (pings the before continuing and, if any of them is unavailable, runs restoration prosedure (see below)) and, in case of file writing, forwards their IPs and access tokens to the Client. Then it stores the virtual address of the new file along with the IPs of the hosts where it is located. It also updates the available capacity of these two hosts. For file creation NameServer just send a command to the chosen servers to create new file with given name and all of the next steps are not performed.
3. Client connects to the FileServers and transfers the file to the respective dirrectories.
4. File name collisions. In case Client attempts to create/upload file that already exists in the DFS, the NameServer will ask if the user wants to update the existing file, or override it.
5. After the Client finishes transferring the file, it sends an acknowledgement to the NameServer.

### Create a new directory
1. Run `make_dir` command on the Client machine to create a new directory in the current directory.
2. Client sends a message to the NameServer with the request for allocation. It passes the name of the directory along with it. 
3. NameServer sends messages to **every** FileServer, indicating which new directory needs to be created. And where.

### Delete an existing file/direcory
1. User runs `delete_file` or `delete_dir` command that sends a request, specifying the virtual path to the file/directory which is to be deleted. 
2. NameServer sends messages to the FileServers that are storing this file that it needs to be deleted and removes it from the Map.
3. In case the user wants to delete a directory, NameServer should send this message to every FileServer as well as ask the user for confirmation if the directory is not empty. If another user is currently located in this directory, or any of its subdirectories, then siplmy restrict deleting it. 

### Read (download to local storage) an existing file
1. User runs `read_file`, Client asks it to provide the name which should be the path to the file which exists in the current directory. Client starts listening for incoming file transmition, requests and sends the message to the NameServer, indicating the name of the file it wants to receive along with the port number on which it is listening.
2. NameServer randomly picks a FileServer which has the needed file and asks it to transmit the file to the Client which in turn stores the received file to the path, specified by the user.

### Open directory
1. Run `open_dir` command to open the directory specified by the user. Client sends the message to the NameServer with the request. If everything is okay, then it changes the localy stored info about current directory of the given Client. Instead of directory name you can type `../` and it will go to the parent directory.

### File info
1. Command `info_file` allows to know info about a file. Client sends the request to the NameServer which returns the subblock of the Map, dedicated to the file.
- Read directory. Command `list_dir` returns the list of names of the contents of the current directory.

## Emergency situations

### NameServer crash
1. If our NameServer crashes, then some of the operations still might be performed. Every time the client writes somethig to DFS it receives the IP's and ports of the FileServers. Client can cache them. If Client needs to create or upload a file, it first should send a message to the NameServer, which should choose two FileServers and pass then to the Client. In case there's no responce from the NameServer, Client can try to send the file itself. It will pick two servers from the cached list at random. If any of them doesn't reply, try another from the list. If it is not able to find two working servers, then it shouldn't send files at all. Instead, it will inform user, that the NameServer is unavailable and it's better to ask his/her sysadmin to check it.
2. If we manage to relounch the NameServer again, then it will simply pull all the needed data from the FileServers, where it is backuped. It is done at the initial lounch as well, but most of the data structures are simply empty. Now, our newly resurrected NameServer has the Map and the info about the FileServers. The only problem is the list of trusted hosts. And that's why there's the caching of clients at the NameServer - it remebers the IPs, so there's no need to copy and paste the token again.

### FileServer crash
1. If one of the FileServers crashes, it will be known after a Client tries to upload something to it or read a file from it. Before every interaction with any of the FileServers NameServers pings them, and if there's no reply from the FileServer, it is considered to be dead. In this case, NameServer should go through all the files that were stored on the dead server, find the other server that stores the copy of one of these files and make it transfer it to one of the running servers.
2. If the FileServer resurrects, then it is linked to the system according to the protocol for adding a new FileServer (see below) i.e. as a brand new.

## Adding machines to the system

### Adding new Clients.
1. From the user point of view this process isn't very different from creating the very first Client. User must obtain the auth token for thr NameServer and send the request. The only difference is that now there's no need in creating the root directory.
2. NameServer adds the new Client to the list of the trusted hosts and sets it's location in the DFS (root dir).

### Adding new FileServers
1. First, launch the new FileServer and get the auth token.
2. Next, connect to it from the NameServer using that token. Now it will be added to the list of availabe FileServers and utilized together with the others.
3. Since we do not create the system from scratch and do not restart the NameServer, it will omit the empty Map received from the newly created FileServer. Instead, it will push existing Map to that server, which will parse it and create all the directories.

## Data structures in use

### Map
1. Map is an auxillary data structure which is stored at NameServer and used to map the virtual adresses to real ones on the FileServers.
2. It is backed-up to all the FileServers after every update and pulled by the NameServer after creation. Initially, its value is `null`.
2. Example:
``` javascript
    {
        name: 'root',
        contents: [
            {
                name: 'usr',
                contents: [
                    {
                        name: 'bin',
                        contents: null
                    },
                    {
                        name: 'duck.png',
                        size: 15,
                        servers: [
                            '14.88.14.88:666',
                            '69.69.69.69:88005553535'
                        ]
                    }
                ]
            },
            {
                name: 'index.ts',
                size: 2,
                servers: [
                    '1.1.1.1:1',
                    '2.2.2.2:2'
                ]
            }
        ]
    }
```
### List of trusted hosts
1. This data structure is only stored at the NameServer. Initially it is empty and is updated everytime a new Client connects. 
2. Each node stores the IP of the Clienta and its position inside the file system tree, e.g. `/root/bin/code`.

### List of FileServers
1. This list is also stored only on the NameServer, but its pieces can be found at different parts of the system. 
2. It contains IP and the port for the system messages (commands from the NameServer), available storage left (initial ammount is specified at the lounching process at each of the FileServers) and auth token.

### Caches
1. As already mentioned, Client caches the IPs, ports and tokens of the most recent FileServers and can use them in case of NameServer crash.
2. Also, FileServer caches the NameServer, so it can be easily reconnected.
3. And NameServer cahces Clients for the sane reason.
4. It can always be cleared at any moment.
