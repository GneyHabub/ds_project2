import {Directory, File, isDir, isFile, StorageGlobalObject} from '../common/types';
import path from 'path';
import fs from 'fs';
import fetch from 'node-fetch';
import ora = require('ora');

export const global: StorageGlobalObject = {
  storage_path: null,
  ip: null,
  port: null,
  token: null,

  ns_ip: null,
  capacity: null,
  map: null,
  servers: [],
};

export const cache = {
  cachePath: path.join(__dirname, 'cache.txt'),

  // Get the global object from the cache.txt file and apply it to the same object
  get (exists: (contents: StorageGlobalObject) => void, notExists: () => void) {
    if (fs.existsSync(this.cachePath)) {
      const contents: StorageGlobalObject = JSON.parse(fs.readFileSync(this.cachePath, 'utf8'));
      Object.keys(global).forEach((key) => {
        // @ts-ignore
        if (contents[key]) global[key] = contents[key];
      });
      exists(contents);
    } else {
      notExists();
    }
  },

  // Set active global object to the cache.txt file
  set () {
    fs.writeFileSync(this.cachePath, JSON.stringify(global));
  },
};

const traverseDir = async (dir: Directory, pathBefore: string) => {
  const newBefore = pathBefore + '/' + dir.name;
  const dirPath = path.join((global.storage_path || ''), newBefore);
  const myAddress = `${global.ip}:${global.port}`;

  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
  }

  const files = dir.contents.filter((something) => isFile(something)) as File[];
  const dirs = dir.contents.filter((something) => isDir(something)) as Directory[];

  await Promise.all(files.map(async (file) => {
    if (file.servers.length < 2 && !file.servers.includes(myAddress)) {
      const serverIp = file.servers[0];
      await fetch(`http://${serverIp}/read-file-storage`, {
        method: 'POST',
        body: JSON.stringify({
          token: (global.servers.find(server => server.ip === serverIp) || {}).token,
          path: newBefore,
          name: file.name,
        }),
      }).then(res => new Promise((resolve, reject) => {
        const dest = fs.createWriteStream(global.storage_path + newBefore + '/' + file.name);
        res.body.pipe(dest);
        dest.on('error', () => {
          reject();
        });
        res.body.on('end', () => {
          file.servers.push(myAddress);
          resolve();
        });
      }));
    }
  }));

  await Promise.all(dirs.map(async (dirToTraverse) => {
    await traverseDir(dirToTraverse, newBefore);
  }));
};

export const traverseMap = async () => {
  if (global.map) {
    const spinner = ora();
    spinner.text = 'Restoring the file structure and replicating endangeder files. It might take a while.';
    spinner.start();
    await traverseDir(global.map.rootDir, '');
    spinner.stop();
  }
};
