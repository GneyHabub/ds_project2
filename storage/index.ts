import { createServer, IncomingMessage, ServerResponse } from 'http';
import {Answer, FileServerRecord, Map} from '../common/types';
import hashString from '../common/hash-string';
import fs from 'fs';
import { bold, red } from 'kleur';
import {prompt} from 'enquirer';
import fetch from 'node-fetch';
import parse from '../common/request-parse';
import path from 'path';
import getFreePort from '../common/get-free-port';
import requestHandle from '../common/request-handle';
import getDirSize from '../common/get-dir-size';
import getIp from '../common/get-ip';
import ora from 'ora';
import {cache, global, traverseMap} from './global';
import readFile from '../common/read-file';

process.setMaxListeners(1000);

// Initialize cached global object
cache.get(() => null, () => null);

global.token = hashString(12);
global.storage_path = path.join(__dirname, '..', 'disk');
global.port = null;
global.ip = getIp();
cache.set();

(async function init() {
  console.log(bold().yellow('Initialization success.'));

  if (!global.capacity) {
    const response: Answer = await prompt({
      type: 'input',
      name: 'capacity',
      message: 'Set the storage capacity (Mb) for this FileServer:',
    });
    global.capacity = parseInt(response.capacity) * 1024 * 1024;
    cache.set();
  }

  if (global.storage_path && !fs.existsSync(global.storage_path)){
    fs.mkdirSync(global.storage_path);
  }

  getFreePort((port) => {
    global.port = port;
    server.listen(port,  '0.0.0.0', () => {
      console.log(`
Initialization success.
File server is running on ${global.ip}:${port}
The token is: ${global.token}
`);
    });
  });
})();

function move(oldPath: string, newPath: string, callback: () => void) {

  fs.rename(oldPath, newPath, function (err) {
      if (err) {
          if (err.code === 'EXDEV') {
              copy();
          } else {
              console.log(err.code);
          }
          return;
      }
      callback();
  });

  function copy() {
      var readStream = fs.createReadStream(oldPath);
      var writeStream = fs.createWriteStream(newPath);

      readStream.on('error', callback);
      writeStream.on('error', callback);

      readStream.on('close', function () {
          fs.unlink(oldPath, callback);
      });

      readStream.pipe(writeStream);
  }
}

const server = createServer((request: IncomingMessage, response: ServerResponse) => {
  try {
    switch (request.url) {
      case '/ping': {
        requestHandle({
          method: 'POST',
          condition: (body) => body.token && body.token === global.token && body.map,
          400: 'Wrong token or token not provided',
        }, request, response, async (body: {map: Map, token: string, servers: FileServerRecord[]}) => {
          global.map = body.map;
          global.servers = body.servers;
          cache.set();

          response.end(JSON.stringify({
            capacity: global.capacity,
            free: (global.capacity || 0) - getDirSize(global.storage_path || ''),
          }));
        });
        break;
      }

      case '/ns-auth': {
        if (request.method === 'POST') {
          parse(request, response, async (body: {map: Map, token: string, servers: FileServerRecord[]}) => {
            if (body.token && body.token === global.token && body.map) {
              console.log('NameServer connected with IP: ' + request.connection.remoteAddress);

              // Cache the NameServer IP
              global.ns_ip = request.connection.remoteAddress as string;

              global.map = body.map;
              global.servers = body.servers;
              await traverseMap();
              cache.set();

              response.statusCode = 200;
              response.end(JSON.stringify({
                capacity: global.capacity,
                free: (global.capacity || 0) - getDirSize(global.storage_path || ''),
                map: global.map,
              }));
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token or token not provided'}));
            }
          });
        }
        break;
      }

      case '/ns-init': {
        requestHandle({
          method: 'POST',
          condition: (body) => body.map,
        }, request, response, async (body: {map: Map}) => {
          if (request.connection.remoteAddress === global.ns_ip) {
            console.log('NameServer connected with IP: ' + request.connection.remoteAddress);

            global.map = body.map;
            cache.set();

            response.statusCode = 200;
            response.end(JSON.stringify({
              ip: `${global.ip}:${global.port}`,
              token: global.token,
              capacity: global.capacity,
              free: (global.capacity || 0) - getDirSize(global.storage_path || ''),
            }));
          } else {
            response.statusCode = 400;
            response.end();
          }
        });
        break;
      }

      case '/create-dir': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token === global.token) {
              if (!fs.existsSync(global.storage_path + body.path + '/' + body.dirName)){
                fs.mkdirSync(global.storage_path + body.path + '/' + body.dirName, { recursive: true });
                console.log(`Created new virtual directory ${body.dirName} at ${body.path ? body.path : 'root'}`);
                response.statusCode = 204;
                response.end();
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such directory already exists!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Kto vi? Ya vas ne znay! Idite nahoi!'}));
            }
          });
        }
        break;
      }

      case '/delete-dir': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token === global.token) {
              if (fs.existsSync(global.storage_path + body.path + '/' + body.dirName)){
                fs.rmdirSync(global.storage_path + body.path + '/' + body.dirName, { recursive: true });
                console.log(`Deleted virtual directory ${body.dirName} at ${body.path ? body.path : 'root'}`);
                response.statusCode = 204;
                response.end();
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such directory doesn\'t exists!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Kto vi? Ya vas ne znay! Idite nahoi!'}));
            }
          });
        }
        break;
      }

      case '/write-file': {
        try {
          if (request.method === 'POST') {
            const filename = request.headers['filename'] as string || '';
            const filesize = request.headers['filesize'] as string || '';
            const clientpath = request.headers['clientpath'] as string || '';
            const servertoken = request.headers['servertoken'] as string || '';
            if (filename && filesize && clientpath && servertoken && servertoken === global.token) {
              const stream = fs.createWriteStream(global.storage_path + clientpath + '/' + filename);
              request.pipe(stream);

              // After all the data is saved, respond with a simple html form so they can post more data
              request.on('end', function () {
                console.log('File is written into ' + clientpath + '/' + filename);
                response.statusCode = 204;
                response.end();
              });

              // This is here in case any errors occur
              stream.on('error', function (err) {
                response.statusCode = 500;
                response.end(JSON.stringify({error: 'An error occurred during file upload:' + err}));
              });
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Proper request headers are not provided'}));
            }
          } else {
            response.statusCode = 404;
            response.end(JSON.stringify({error: 'Wrong url or method of the request'}));
          }
        } catch (e) {
          response.statusCode = 500;
          response.end(JSON.stringify({error: 'An error occurred during file upload:' + e}));
        }
        break;
      }

      case '/create-file': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token && body.token === global.token) {
              if (!fs.existsSync(global.storage_path + body.path + '/' + body.fileName)){
                fs.writeFileSync(global.storage_path + body.path + '/' + body.fileName, '');
                console.log(`Created new file ${body.fileName} at ${body.path ? body.path : 'root'}`);
                response.statusCode = 204;
                response.end();
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such file already exists!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/move-file': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token && body.token === global.token) {
              if (fs.existsSync(global.storage_path + body.oldPath)){
                move(global.storage_path + body.oldPath, global.storage_path + body.newPath, () => {
                  response.statusCode = 204;
                  response.end();
                });
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such file doesn\'t exist!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/copy-file': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token && body.token === global.token) {
              if (fs.existsSync(global.storage_path + body.oldPath)){
                fs.copyFile(global.storage_path + body.oldPath, global.storage_path + body.newPath, (err) => {
                  if (err) throw err;
                  response.statusCode = 204;
                  response.end();
                });
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such file doesn\'t exist!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/delete-file': {
        if (request.method === 'POST') {
          parse(request, response, (body) => {
            if (body.token === global.token) {
              if (fs.existsSync(global.storage_path + body.path + '/' + body.fileName)){
                fs.unlinkSync(global.storage_path + body.path + '/' + body.fileName);
                console.log(`Deleted file ${body.fileName} at ${body.path ? body.path : 'root'}`);
                response.statusCode = 204;
                response.end();
              } else {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Such file doesn\'t exists!'}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/read-file': {
        if (request.method === 'POST') {
          parse(request, response, async (body: {token: string, path: string, name: string, ip: string}) => {
            if (body.token && body.token === global.token) {
              const stream = fs.createReadStream(global.storage_path + body.path + '/' + body.name);

              const spinner = ora();
              spinner.text = 'Uploading the file: ' + body.name;
              spinner.start();

              try {
                console.log('uploading to', body.ip);
                const readFileResp = await fetch(`http://${body.ip}/get-read-file`, {
                  method: 'POST',
                  headers: {
                    filename: body.name,
                  },
                  body: stream,
                });
                spinner.stop();
                if (readFileResp.status === 204) {
                  response.statusCode = 204;
                  response.end();
                } else {
                  const respBody = await readFileResp.json();
                  console.log(red('An error occurred while uploading your file: ' + respBody.error));
                  response.statusCode = readFileResp.status;
                  response.end(JSON.stringify({error: respBody.error}));
                }
              } catch (e) {
                spinner.stop();
                console.log(red('An error occurred while uploading your file: ' + e));
                response.statusCode = 400;
                response.end(JSON.stringify({error: e}));
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/read-file-storage': {
        if (request.method === 'POST') {
          parse(request, response, async (body: {token: string, path: string, name: string}) => {
            console.log('uploading', body.path, body.name);
            if (body.token && body.token === global.token) {
              const stream = fs.createReadStream(global.storage_path + body.path + '/' + body.name);
              stream.pipe(response);
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/move-file-to-storage': {
        if (request.method === 'POST') {
          parse(request, response, async (body: {
            token: string,
            toIp: string,
            toToken: string,
            path: string,
            name: string,
          }) => {
            if (body.token && body.token === global.token && body.name) {
              const filePath = body.path + '/' + body.name;
              console.log('Uploading', filePath, 'to', body.toIp);

              const stream = fs.createReadStream(global.storage_path + filePath);
              const res = await fetch(`http://${body.toIp}/get-file-from-storage`, {
                method: 'POST',
                headers: {
                  filename: body.name,
                  clientpath: body.path,
                  servertoken: body.toToken,
                },
                body: stream,
              });

              if (res.ok) {
                response.statusCode = 204;
                response.end();
              } else {
                response.statusCode = 400;
                response.end();
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Wrong token is provided!'}));
            }
          });
        }
        break;
      }

      case '/get-file-from-storage': {
        if (request.method === 'POST') {
          const filename = request.headers['filename'] as string || '';
          const clientpath = request.headers['clientpath'] as string || '';
          const servertoken = request.headers['servertoken'] as string || '';
          if (filename && clientpath && servertoken && servertoken === global.token) {
            const stream = fs.createWriteStream(global.storage_path + clientpath + '/' + filename);
            request.pipe(stream);

            request.on('end', function () {
              console.log(`File ${filename} is successfully replicated from another file server`);
              response.statusCode = 204;
              response.end();
            });

            // This is here in case any errors occur
            stream.on('error', function (err) {
              response.statusCode = 500;
              response.end(JSON.stringify({error: 'An error occurred during file upload:' + err}));
            });
          }
        }
        break;
      }

      case '/get-read-file': {
        console.log('storage receive file', request.headers);
        // The function is common because it is also used in the file server
        readFile(request, response, global.storage_path || '');
        break;
      }

      default: {
        response.statusCode = 404;
        response.end();
      }
    }
  } catch (e) {
    console.error('Some error happened.\n', e, 'Try again.');
  }
});
