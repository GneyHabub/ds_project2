import {Answer, FileServerRecord, ServerGlobalObject, File, Directory, isDir } from '../common/types';
import {prompt} from 'enquirer';
import fetch from 'node-fetch';
import { bold, red } from 'kleur';
import {global, removeDeadServer} from './global';
import requestError from '../common/request-error';
import pauseUntilAnykey from '../common/pause-until-anykey';

import fs from 'fs';
import path from 'path';
import {yellow} from 'kleur/colors';

export const cache = {
  cachePath: path.join(__dirname, 'cache.txt'),
  // Get the global object from the cache.txt file and apply it to the same object
  get (exists: (contents: ServerGlobalObject) => void, notExists: () => void) {
    if (fs.existsSync(this.cachePath)) {
      const contents: ServerGlobalObject = JSON.parse(fs.readFileSync(this.cachePath, 'utf8'));
      Object.keys(global).forEach((key) => {
        // @ts-ignore
        if (contents[key]) global[key] = contents[key];
      });
      exists(contents);
    } else {
      notExists();
    }
  },
  // Set active global object to the cache.txt file
  set () {
    fs.writeFileSync(this.cachePath, JSON.stringify(global));
  },
}

export const initialInit = async () => {
  cache.get(async () => {
    const responses: (FileServerRecord|null)[] = await Promise.all(global.fileServers.map(async (server) => {
      try {
        const res = await fetch(`http://${server.ip}/ns-init`, {
          method: 'POST',
          body: JSON.stringify({map: global.map, servers: global.fileServers}),
        });
        if (res.ok && res.status === 200) {
          return await res.json();
        } else {
          return null;
        }
      } catch (_) {
        return null;
      }
    }));

    const servers = responses.filter(server => !!server) as FileServerRecord[];
    if (servers.length) {
      console.log(bold().yellow().italic(`Auto connected ${servers.length} file servers.`));
      global.fileServers = servers;

      cache.set();
      console.table(global.fileServers);
      await operations.connectClient();
    } else {
      await operations.addFileServer();
      console.table(global.fileServers);
    }
  }, async () => {
    await operations.addFileServer();
    console.table(global.fileServers);
  });
};

export const operations = {
  connectClient: async () => {
    operations.logConnectionData();
  },

  addFileServer: async () => {
    const res1: Answer = await prompt({
      type: 'input',
      name: 'ip',
      message: 'Type the ip address and the port of the FileServer:',
      validate: (input) => !!input,
    });
    const res2: Answer = await prompt({
      type: 'input',
      name: 'token',
      message: 'Type the token provided by the FileServer:',
      validate: (input) => !!input,
    });

    const response = await fetch(`http://${res1.ip}/ns-auth`, {
      method: 'POST',
      body: JSON.stringify({token: res2.token, map: global.map, servers: global.fileServers}),
    });

    await requestError(response, (body) => {
      global.fileServers.push({
        ip: res1.ip,
        token: res2.token,
        capacity: body.capacity,
        free: body.free,
      });

      if (body.map) global.map = body.map;

      // Cache all file servers in the root of the name server
      cache.set();
      console.log(`Connected! New FileServer at ${res1.ip}`);
    });
    pauseUntilAnykey(performOperations);
  },

  logConnectionData: () => {
    console.log(yellow(`
The local IP and port for client connection are: ${bold(global.ip + ':' + global.PORT)}
The auth token is: ${bold(global.TOKEN || '')}
    `));
    pauseUntilAnykey(performOperations);
  },

  listFileServers: async () => {
    await pingFileServers();
    console.table(global.fileServers);
    pauseUntilAnykey(performOperations);
  },

  listCLients: () => {
    console.table(global.trustedHosts);
    pauseUntilAnykey(performOperations);
  }
}

process.on('uncaughtException', function (err) {
  pauseUntilAnykey('An error happened: ' + err, performOperations);
});
process.on('unhandledRejection', (err) => {
  pauseUntilAnykey('An error happened: ' + err, performOperations);
});

export async function performOperations () {
  const response: { command: keyof typeof operations } = await prompt({
    type: 'autocomplete',
    name: 'command',
    message: 'Command:',
    initial: 0,
    choices: Object.keys(operations),
  });

  await operations[response.command]();
}

export async function pingFileServers () {
  await Promise.all(global.fileServers.map(async (server) => {
    try {
      const response = await fetch(`http://${server.ip}/ping`, {
        method: 'POST',
        body: JSON.stringify({token: server.token, map: global.map}),
      });
      await requestError(response, (body: {capacity: number, free: number}) => {
        server.free = body.free;
        server.capacity = body.capacity;
      });
    } catch (_) {
      await removeDeadServer(server.ip);
    }
  }));

  cache.set();
}

export async function createDir (path: string, name: string) {
  await Promise.all(global.fileServers.map(async (server: FileServerRecord) => {
    const response = await fetch(`http://${server.ip}/create-dir`, {
      method: 'POST',
      body: JSON.stringify({token: server.token, path: path, dirName: name}),
    });
    if (response.status !== 204) {
      try {
        const body = await response.json();
        console.log(red(body.error));
      } catch (_) {
        console.log('Something wrong happened.');
      }
    } else {
      if (path) {
        findDir(path).contents.push({
          name: name,
          contents: [],
        });
      }
      console.log(`Created new virtual directory ${name} at ${path ? path : 'root'}`);
    }
  }));
  cache.set();
}

export async function deleteDir(path: string, name: string) {
  await Promise.all(global.fileServers.map(async (server: FileServerRecord) => {
    const response = await fetch(`http://${server.ip}/delete-dir`, {
      method: 'POST',
      body: JSON.stringify({token: server.token, path: path, dirName: name}),
    });
    if (response.status !== 204) {
      try {
        const body = await response.json();
        console.log(body.error);
      } catch (_) {
        console.log('Something wrong happened.');
      }
    } else {
      findDir(path).contents = findDir(path).contents.filter(el => el.name !== name);
      console.log(`Deleted virtual directory ${name} at ${path ? path : 'root'}`);
    }
  }));
  cache.set();
}

export function findDir(path: string): Directory {
  const pathTokens = path.split('/');
  pathTokens.shift();
  let tmpDir: Directory = global.map.rootDir;
  if (tmpDir.name === pathTokens[0] && pathTokens.length === 1) {
    return tmpDir;
  }
  pathTokens.shift();
  pathTokens.forEach(dir => {
    let nextDir = tmpDir.contents.find(el1 => isDir(el1) && el1.name === dir);
    if (nextDir && isDir(nextDir)) {
      tmpDir = nextDir;
    }
  });
  return tmpDir;
}

export async function getEmptiestFileservers (): Promise<(FileServerRecord)[]> {
  await pingFileServers();
  global.fileServers.sort((a, b) => b.free - a.free);
  return global.fileServers.length > 1 ? [global.fileServers[0], global.fileServers[1]] : [global.fileServers[0]];
}
