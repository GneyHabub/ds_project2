import {
  ActiveProcess,
  FileServerRecord,
  ServerGlobalObject,
  Directory,
  isFile, File, isDir
} from '../common/types';
import fetch from 'node-fetch';
import {cache} from './operations';
import {yellow, red} from 'kleur';

export const global: ServerGlobalObject = {
  map: {
    rootDir : {
      name: '',
      contents: [],
    },
  },
  PORT: null,
  ip: null,
  TOKEN: null,
  fileServers: [],
  trustedHosts: [],
};

export const activeProcesses: ActiveProcess[] = [];

const reassignFiles = async (dir: Directory, pathBefore: string, deadServerIp: string) => {
  const newBefore = pathBefore + '/' + dir.name;

  const files = dir.contents.filter((something) => isFile(something)) as File[];
  const dirs = dir.contents.filter((something) => isDir(something)) as Directory[];

  await Promise.all(files.map(async (file) => {
    if (file.servers.includes(deadServerIp)) {
      file.servers = file.servers.filter(server => server !== deadServerIp);
      if (file.servers.length === 1 && global.fileServers.length > 1) {
        const moveFrom = global.fileServers.find(server => server.ip === file.servers[0]) as FileServerRecord;
        const restFS = global.fileServers.filter(server => server.ip !== moveFrom.ip);
        restFS.sort((a, b) => b.free - a.free);
        const moveTo = restFS[0];

        if (moveFrom && moveTo) {
          const res = await fetch(`http://${moveFrom.ip}/move-file-to-storage`, {
            method: 'POST',
            body: JSON.stringify({
              token: moveFrom.token,
              toIp: moveTo.ip,
              toToken: moveTo.token,
              path: newBefore,
              name: file.name,
            }),
          });
          if (res.ok) {
            file.servers.push(moveTo.ip);
          } else {
            console.log(red(`File ${newBefore + '/' + file.name} is lost.`))
          }
        }
      }
    }
  }));

  await Promise.all(dirs.map(async (dirToTraverse) => {
    await reassignFiles(dirToTraverse, newBefore, deadServerIp);
  }));
};

export const removeDeadServer = async (deadServerIp: string) => {
  console.log(yellow().italic(`File server ${deadServerIp} is disconnected. All its files would be moved.`))
  global.fileServers = global.fileServers.filter(server => server.ip !== deadServerIp);
  await reassignFiles(global.map.rootDir, '', deadServerIp);
  cache.set();
};
