import { createServer, IncomingMessage, ServerResponse } from 'http';
import { bold, red } from 'kleur';
import {isDir, isFile, File, ClientRecord} from '../common/types';
import {initialInit, createDir, findDir, getEmptiestFileservers, operations, deleteDir, cache} from './operations'
import fetch from 'node-fetch';
import hashString from '../common/hash-string';
import {activeProcesses, global} from './global';
import requestHandle from '../common/request-handle';
import getIp from '../common/get-ip';

global.PORT = 3000;
global.TOKEN = hashString(12);
global.ip = getIp()

process.setMaxListeners(1000);

console.log(bold().yellow().italic(`
Initialization success.
NameServer is running on ${global.ip}:${global.PORT}.
`));

(async function () {
  await initialInit();
})();

const server = createServer((request: IncomingMessage, response: ServerResponse) => {
  switch (request.url) {
    case '/client-auth': {
      requestHandle({
        method: 'POST',
        condition: body => body.token && body.token === global.TOKEN,
        400: 'Wrong token or token not provided'
      }, request, response, (body) => {
        console.log('Client connected with IP: ' + request.connection.remoteAddress);
        response.statusCode = 200;
        if (global.trustedHosts.length === 0) {
          response.end(JSON.stringify({firstUser: true}));
          console.log('First client is connected. Time to create the root directory of the DFS.');
        } else {
          response.end(JSON.stringify({firstUser: false, rootDir: global.map.rootDir.name}));
          global.trustedHosts.push({
            ip: request.connection.remoteAddress || '',
            username: body.username,
            position: '/' + global.map.rootDir.name,
          });
          console.table(global.trustedHosts);
        }
        cache.set();
      });
      break;
    }

    case '/client-re-auth': {
      requestHandle({
        method: 'POST',
        condition: () => true,
      }, request, response, () => {
        const clientIp = request.connection.remoteAddress

        if (global.trustedHosts.some(client => client.ip === clientIp)) {
          const client = global.trustedHosts.find(client => client.ip === clientIp) as ClientRecord;
          client.position = '/' + global.map.rootDir.name;

          console.log(`Client ${client.username} reconnected.`);

          response.statusCode = 200;
          response.end(JSON.stringify({
            ip: `${global.ip}:${global.PORT}`,
            token: global.TOKEN,
            firstUser: false,
            rootDir: global.map.rootDir.name,
          }));
        } else {
          response.statusCode = 400;
          response.end();
        }
      });
      break;
    }

    case '/create-root-dir': {
      requestHandle({
        method: 'POST',
        condition: body => body.token && body.token === global.TOKEN && body.rootDirName,
      }, request, response, async (body) => {
        global.map = {rootDir: {
          name: body.rootDirName,
          contents: [],
        }};
        await createDir('', body.rootDirName);
        global.trustedHosts.push({
          ip: request.connection.remoteAddress || '',
          username: body.username,
          position: '/' + global.map.rootDir.name,
        });
        console.table(global.trustedHosts);
        cache.set();
        response.statusCode = 204;
        response.end();
      })
      break;
    }

    case '/create-dir': {
      requestHandle({
        method: 'POST',
        condition: body => body.token && body.token === global.TOKEN,
        400: 'Wrong token provided!',
      }, request, response, async (body) => {
        if (body.dirName) {
          const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
          if (client) {
            await createDir(client.position, body.dirName);
          } else {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
          }
          response.statusCode = 204;
          response.end();
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Directory name was not provided!'}));
        }
      });
      break;
    }

    case '/list-dir': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
        400: 'Wrong token provided!',
      }, request, response, (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          response.statusCode = 200;
          response.end(JSON.stringify({contents: findDir(client.position).contents.map(el => {
            return {
              name: el.name,
              isDir: isDir(el),
            }
          })}));
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
        }
      });
      break;
    }

    case '/file-info': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.fileName && body.token && body.token === global.TOKEN,
        400: 'Wrong token provided!',
      }, request, response, (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          const openDir = findDir(client.position);
          const file = openDir.contents.find(el => el.name === body.fileName && isFile(el));
          if (file) {
            response.statusCode = 200;
            response.end(JSON.stringify({name: file.name, size: (file as File).size}));
          } else {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'No such file!'}));
          }
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
        }
      });
      break;
    }

    case '/open-dir': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
        400: 'Wrong token provided!',
      }, request, response, (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          if (body.dirName === '../'){
            const pathTokens = client.position.split('/');
            if (pathTokens.length === 2) {
              response.statusCode = 204;
              response.end();
              return;
            }
            let res = '';
            for (let i = 1; i < pathTokens.length - 1; i++){
              res = res + '/' + pathTokens[i];
            }
            client.position = res;
            response.statusCode = 204;
            response.end();
          } else {
            const parentDir = findDir(client.position);
            const dir = parentDir.contents.find(el => isDir(el) && el.name === body.dirName);
            if (dir) {
              client.position = client.position + '/' + body.dirName;
              response.statusCode = 204;
              response.end();
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'No such directory!'}));
            }
          }
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first.'}));
        }
      });
      break;
    }

    case '/write-file': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.name && body.size > 0 && body.token && body.token === global.TOKEN,
      }, request, response, async (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          const filename = body.name;

          // Check if the file with the same name is already in the active director
          const activeDir = findDir(client.position);
          const hasFile = activeDir.contents.some(file => !isDir(file) && file.name === filename);
          if (hasFile && !body.action) {
              // Require some action from the client (override or copy) if the file exists
              response.end(JSON.stringify({success: false}));
          } else {
            // Get the 2 (or 1) most empty file servers and check if their available space is more than needed
            const servers = await getEmptiestFileservers();
            const enoughMemoryServers = servers.filter(server => server.free > body.size);

            if (enoughMemoryServers.length) {
              if (activeProcesses.some(process => process.client.position === client.position && process.file === filename)) {
                response.statusCode = 400;
                response.end(JSON.stringify({error: 'Somebody is writing to this file at the same time. Try again later.'}))
              } else {
                const processId = hashString(5);

                const result = {
                  success: true,
                  servers,
                  path: client.position,
                  process: processId,
                };

                activeProcesses.push({
                  id: processId,
                  file: filename,
                  size: body.size,
                  started: Date.now(),
                  servers: enoughMemoryServers,
                  client,
                });

                // If file already exists, check if client has sent something that deals with the situation
                if (body.action && body.action === 'override') {
                  response.end(JSON.stringify({
                    ...result,
                    name: filename,
                    override: true,
                  }));
                } else if (body.action && body.action === 'copy') {
                  response.end(JSON.stringify({
                    ...result,
                    name: `${filename}-${hashString(5)}`,
                  }));
                } else {
                  response.end(JSON.stringify({
                    ...result,
                    name: filename,
                  }));
                }
              }
            } else {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Not enough memory on file servers. Remove files or wait for increase in memory.'}));
            }
          }
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
        }
      })
      break;
    }

    case '/write-file-ack': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.servers && body.process && body.token && body.token === global.TOKEN,
      }, request, response, async (body) => {
        const proccess = activeProcesses.find(process => process.id === body.process);
        if (proccess) {
          // Remove active process
          activeProcesses.forEach((proccess, i) => {
            if (proccess.id === body.process) activeProcesses.splice(i, 1);
          });

          // Get an array of servers where the file is successfully pushed
          const successfulServers = (body.servers as {ip: string, success: boolean}[]).filter(server => server.success).map(server => server.ip);
          if (successfulServers.length) {
            // Update Map
            const newFile: File = {
              name: proccess.file,
              size: proccess.size,
              servers: successfulServers,
            };
            findDir(proccess.client.position).contents.push(newFile);
            cache.set();
          }

          response.statusCode = 204;
          response.end();
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: `The process ${body.process} is not found.`}))
        }
      });
      break;
    }

    case '/read-file': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
      }, request, response, async (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          const filename = body.fileName;
          const activeDir = findDir(client.position);
          const file = activeDir.contents.find(file => !isDir(file) && file.name === filename);
          if (!file) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'No such file in current directory!'}));
            return;
          }

          // Choose random server that stores the file
          const server = global.fileServers.find(s => s.ip === (file as File).servers[Math.floor(Math.random() * (file as File).servers.length)])  || global.fileServers[0];

          if (activeProcesses.some(process => process.client.position === client.position && process.file === filename)) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'Somebody is writing to this file at the same time. Try again later.'}))
          } else {
            const processId = hashString(5);

            activeProcesses.push({
              id: processId,
              file: filename,
              size: body.size,
              started: Date.now(),
              servers: [server],
              client,
            });

            const serverResp = await fetch(`http://${server.ip}/read-file`, {
              method: 'POST',
              body: JSON.stringify({token: server.token, name: filename, path: client.position, ip: client.ip + ':' + body.port}),
            });
            if (serverResp.status === 204) {
              activeProcesses.forEach((proccess, i) => {
                if (proccess.id === processId) activeProcesses.splice(i, 1);
              });
              response.statusCode = 204;
              response.end();
            } else {
              const respBody = await serverResp.json();
              console.log(red('An error occurred while uploading your file: ' + respBody.error));
              response.statusCode = serverResp.status;
              response.end(JSON.stringify({error: respBody.error}));
            }
          }
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
        }
      })
      break;
    }

    case '/delete-dir': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
        400: 'Wrong token or token not provided',
      }, request, response, async (body: {token: string, dirName: string, username: string, confirm: boolean}) => {
        if (body.dirName) {
          const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
          if (client) {
            const dirToDelete = findDir(client.position + '/' + body.dirName);
            // If the dir is not empty – require action from the client
            if (dirToDelete.contents.length) {
              if (body.confirm) {
                await deleteDir(client.position, body.dirName);
              } else {
                response.statusCode = 403;
                response.end();
              }
            } else {
              await deleteDir(client.position, body.dirName);
            }
          } else {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
          }
          response.statusCode = 204;
          response.end();
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Directory name was not provided!'}));
        }
      });
      break;
    }

    case '/create-file': {
      requestHandle({
        method: 'POST',
        condition: body => body.token && body.token === global.TOKEN,
        400: 'Wrong token provided!',
      }, request, response, async (body) => {
        if (body.fileName) {
          const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
          if (client) {
            const activeDir = findDir(client.position);
            const hasFile = activeDir.contents.some(file => isFile(file) && file.name === body.fileName);
            if (hasFile) {
              response.statusCode = 400;
              response.end(JSON.stringify({error: 'Such a file already exists!'}));
            } else {
              const servers = await getEmptiestFileservers();
              const res = await Promise.all(servers.map(async (server) => {
                //TODO: check somehow if the server is dead
                const response = await fetch(`http://${server.ip}/create-file`, {
                  method: 'POST',
                  body: JSON.stringify({
                    token: server.token,
                    path: client.position,
                    fileName: body.fileName,
                  }),
                });
                if (response.status === 400) {
                  try {
                    const body = await response.json();
                    return {ok: false, error: body.error, fs_ip: server.ip};
                  } catch (_) {
                    console.log(red('Something wrong happened.'));
                  }
                }
                return {ok: true, error: null, fs_ip: server.ip};
              }));
              let err = '';
              res.forEach(fsRes => {
                if (!fsRes.ok) {
                  console.log(`Something wrong happend at ${fsRes.fs_ip}: ${red(fsRes.error)}`);
                  err = fsRes.error;
                }
              });
              if (err) {
                response.statusCode = 400;
                response.end(JSON.stringify({error: err}));
              } else {
                findDir(client.position).contents.push({
                  name: body.fileName,
                  size: 0,
                  servers: servers.map(server => server.ip),
                });
                cache.set();
                response.statusCode = 204;
                response.end();
              }
            }
          } else {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
          }
          response.statusCode = 204;
          response.end();
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'File name was not provided!'}));
        }
      });
      break;
    }

    case '/move-file': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
        400: 'Wrong token or token not provided',
      }, request, response, async (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
        if (client) {
          const fileToMove = findDir(client.position).contents.find(el => isFile(el) && el.name === body.fileName);
          if (!fileToMove) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'No such file in current dirrectory!'}));
            return;
          }
          let hashName: string = '';
          if (!body.targetDir) {
            hashName = body.fileName.split('.')[0] + `-${hashString(5)}` + '.' + body.fileName.split('.')[1]
          }
          const servers = (fileToMove as File).servers;
          const url: 'move' | 'copy' = body.task;
          const res = await Promise.all(servers.map(async (server) => {
            //TODO: check somehow if the server is dead
            const serverRecord = global.fileServers.find(s => s.ip === server);
            let token = '';
            if (serverRecord) token = serverRecord.token;
            const serverResp = await fetch(`http://${server}/${url}-file`, {
              method: 'POST',
              body: JSON.stringify({
                token: token,
                oldPath: client.position + '/' + body.fileName,
                newPath: body.targetDir ? body.targetDir  + '/' + body.fileName : client.position + '/' + hashName,
              }),
            });
            if (serverResp.status === 400) {
              try {
                const body = await serverResp.json();
                return {ok: false, error: body.error, fs_ip: server};
              } catch (_) {
                console.log(red('Something wrong happened.'));
              }
            }
            return {ok: true, error: null, fs_ip: server};
          }));
          let err = '';
          res.forEach(fsRes => {
            if (!fsRes.ok) {
              console.log(`Something wrong happened at ${fsRes.fs_ip}: ${red(fsRes.error)}`);
              err = fsRes.error;
            }
          });
          if (err) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: err}));
          } else {
            if (body.task === 'move') {
              findDir(client.position).contents = findDir(client.position).contents.filter(el => el.name !== body.fileName);
            }
            findDir(body.targetDir).contents.push({
              name: body.targetDir ?  fileToMove.name : hashName,
              size: (fileToMove as File).size,
              servers: (fileToMove as File).servers,
            });
            cache.set();
            response.statusCode = 204;
            response.end();
          }
        } else {
          response.statusCode = 400;
          response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
        }
      });
      break;
    }

    case '/delete-file': {
      requestHandle({
        method: 'POST',
        condition: (body) => body.token && body.token === global.TOKEN,
        400: 'Wrong token or token not provided',
      }, request, response, async (body) => {
        const client = global.trustedHosts.find(host => host.ip === request.connection.remoteAddress && host.username === body.username);
          if (client) {
            const fileToDelete = findDir(client.position).contents.find(el => isFile(el) && el.name === body.fileName);
          if (!fileToDelete) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'No such file in current dirrectory!'}));
            return;
          }
          const servers = (fileToDelete as File).servers;
          const res = await Promise.all(servers.map(async (server) => {
            //TODO: check somehow if the server is dead
            const serverRecord = global.fileServers.find(s => s.ip === server);
            let token = '';
            if (serverRecord) token = serverRecord.token;
            const serverResp = await fetch(`http://${server}/delete-file`, {
              method: 'POST',
              body: JSON.stringify({
                token: token,
                path: client.position,
                fileName: body.fileName,
              }),
            });
            if (serverResp.status === 400) {
              try {
                const body = await serverResp.json();
                console.log(red(body.error));
                return {ok: false, error: body.error, fs_ip: server};
              } catch (_) {
                console.log(red('Something wrong happened.'));
              }
            }
            return {ok: true, error: null, fs_ip: server};
          }));
          let err: string | null = null;
          res.forEach(fsRes => {
            if (!fsRes.ok) {
              console.log(`Something wrong happend at ${fsRes.fs_ip}: ${red(fsRes.error)}`);
              err = fsRes.error;
            }
          });
          if (err) {
            response.statusCode = 400;
            response.end(JSON.stringify({error: err}));
          } else {
            findDir(client.position).contents = findDir(client.position).contents.filter(el => el.name !== body.fileName);
            cache.set();
            response.statusCode = 204;
            response.end();
          }
          } else {
            response.statusCode = 400;
            response.end(JSON.stringify({error: 'Token is correct, but client is not registered! Register the client first!'}));
          }
          response.statusCode = 204;
          response.end();
      });
      break;
    }

    default: {
      response.statusCode = 404;
      response.end();
    }
  }
});

server.listen(global.PORT, '0.0.0.0');
